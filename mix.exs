defmodule OpenScraper.MixProject do
  use Mix.Project

  def project do
    [
      app: :open_scraper,
      version: "0.1.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.8"},
      {:jason, "~> 1.1"},
      # {:wallaby, "~> 0.22"},
      {:meeseeks, "~> 0.16.1"},
      {:csv, "~> 2.3"},
      {:xlsx_reader, "~> 0.1.0"}
    ]
  end

  defp aliases do
    [
      scrape: ["run -e 'OpenScraper.run'"],
      scrape_2017: ["run -e 'OpenScraper.Older.run'"]
    ]
  end
end
