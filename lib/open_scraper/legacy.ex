defmodule OpenScraper.Legacy do
  @moduledoc "Scrap Legacy CF Games Open"

  import Meeseeks.CSS

  # @years 12..16
  # request  @years 14..18
  @year 16
  @default_timeout 60_000
  @divisions [
    %{id: 1, name: "men"},
    %{id: 2, name: "women"},
    %{id: 7, name: "men55"},
    %{id: 8, name: "women55"},
    %{id: 9, name: "men60"},
    %{id: 10, name: "women60"},
    %{id: 14, name: "boys14"},
    %{id: 15, name: "girls14"},
    %{id: 16, name: "boys16"},
    %{id: 17, name: "girls16"}
  ]

  def run do
    Enum.each(@divisions, &build_data(&1))
  end

  def find_total_pages(url) do
    HTTPoison.get!(url, timeout: @default_timeout).body
    |> Meeseeks.one(css("#leaderboard-pager"))
    |> Meeseeks.one(css("a.button:first-child"))
    |> Meeseeks.text()
    |> String.to_integer()
  end

  def fetch_page_data(page) do
    page
    |> Meeseeks.all(css("#lbtable tr:not(:first-child)"))
    |> Enum.map(fn row ->
      [rank, athlete, event1, event2, event3, event4, event5] = row |> Meeseeks.all(css("td"))

      # [rank, athlete, event1, event2, event3, event4, event5, event6] =
      row |> Meeseeks.all(css("td"))

      athlete_link = Meeseeks.one(athlete, css("a"))
      link = athlete_link |> Meeseeks.attr("href")
      athlete_data = fetch_athlete_data(link)

      %{
        rank: Meeseeks.text(rank),
        event1: Meeseeks.text(event1),
        # event1_tooltip: event1 |> Meeseeks.one(css(".tooltip")) |> Meeseeks.text(),
        event2: Meeseeks.text(event2),
        event3: Meeseeks.text(event3),
        event4: Meeseeks.text(event4),
        event5: Meeseeks.text(event5),
        # event6: Meeseeks.text(event6),
        name: athlete_link |> Meeseeks.text(),
        link: link
      }
      |> Map.merge(athlete_data)
    end)
  end

  defp fetch_athlete_data(link) do
    case HTTPoison.get(link, [timeout: @default_timeout], follow_redirect: true) do
      {:ok, response} ->
        response.body
        |> athlete_data()

      {:error, reason} ->
        IO.inspect("#{link} - #{inspect(reason)}")
        %{}
    end
  end

  defp athlete_data(page) do
    Meeseeks.all(page, css("ul.infobar li .text"))
    |> normalize_athlete_data()
  end

  defp normalize_athlete_data([]), do: %{}

  defp normalize_athlete_data([a, b, c, d, e, f, g]) do
    %{
      z0: Meeseeks.text(a),
      z1: Meeseeks.text(b),
      z2: Meeseeks.text(c),
      z3: Meeseeks.text(d),
      z4: Meeseeks.text(e),
      z5: Meeseeks.text(f),
      z6: Meeseeks.text(g)
    }
  end

  defp normalize_athlete_data(error) do
    IO.inspect("COULD NOT NORMALIZE ATHLETE DATA #{inspect(error)}")
    %{}
  end

  defp build_data(division) when is_map(division) do
    file = File.open!("#{division.name}-#{@year}.csv", [:write, :utf8])

    total_pages = division.id |> url(@year, 1) |> find_total_pages()

    for current_page <- 1..total_pages do
      # for current_page <- 1..2 do
      url = url(division.id, @year, current_page)

      HTTPoison.get!(url, timeout: @default_timeout).body
      |> fetch_page_data()
    end
    |> List.flatten()
    |> Enum.map(&Map.values/1)
    |> CSV.encode()
    |> Enum.each(&IO.write(file, &1))
  end

  defp url(division_id, year, page \\ 1) do
    "https://games.crossfit.com/scores/leaderboard.php?stage=5&sort=0&page=#{page}&division=#{
      division_id
    }&region=0&numberperpage=100&competition=0&frontpage=0&expanded=1&year=#{year}&full=1&showtoggles=0&hidedropdowns=1&showathleteac=1&=&is_mobile=&scaled=0&fittest=1&fitSelect=0&regional=5&occupation=0"
  end
end
