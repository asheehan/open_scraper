defmodule OpenScraper.Open2021 do
  @moduledoc """
  Scrap CF Games Open data from
  https://games.crossfit.com/leaderboard/open/2019?country_champions=0&division=1&sort=0&scaled=0&page=1

  post run data cleanup:
  > find data/2014/ -size 0 -delete
  """

  @year 2021
  # @year 2024
  # older divisions for 2014..2018
  @divisions [
    %{id: 1, scaled: true, name: "men"},
    %{id: 2, scaled: true, name: "women"},
    %{id: 12, scaled: true, name: "men40"},
    %{id: 13, scaled: true, name: "women40"},
    %{id: 18, scaled: true, name: "men35"},
    %{id: 19, scaled: true, name: "women35"},
    %{id: 12, scaled: true, name: "men40"},
    %{id: 13, scaled: true, name: "women40"},
    %{id: 3, scaled: true, name: "men45"},
    %{id: 4, scaled: true, name: "women45"},
    %{id: 5, scaled: true, name: "men50"},
    %{id: 6, scaled: true, name: "women50"},
    %{id: 7, scaled: true, name: "men55"},
    %{id: 8, scaled: true, name: "women55"},
    %{id: 36, scaled: true, name: "men60"},
    %{id: 37, scaled: true, name: "women60"},
    %{id: 38, scaled: true, name: "men65"},
    %{id: 39, scaled: true, name: "women65"},
  ]
  @og_divisions [
    %{id: 1, scaled: true, name: "men"},
    %{id: 2, scaled: true, name: "women"},
    # %{id: 11, scaled: false, name: "team"},
    %{id: 16, scaled: true, name: "boys16"},
    %{id: 17, scaled: true, name: "girls16"},
    %{id: 14, scaled: true, name: "boys14"},
    %{id: 15, scaled: true, name: "girls14"},
    %{id: 18, scaled: true, name: "men35"},
    %{id: 19, scaled: true, name: "women35"},
    %{id: 12, scaled: true, name: "men40"},
    %{id: 13, scaled: true, name: "women40"},
    %{id: 3, scaled: true, name: "men45"},
    %{id: 4, scaled: true, name: "women45"},
    %{id: 5, scaled: true, name: "men50"},
    %{id: 6, scaled: true, name: "women50"},
    %{id: 7, scaled: true, name: "men55"},
    %{id: 8, scaled: true, name: "women55"},
    %{id: 36, scaled: true, name: "men60"},
    %{id: 37, scaled: true, name: "women60"},
    %{id: 38, scaled: true, name: "men65"},
    %{id: 39, scaled: true, name: "women65"},
    %{id: 20, scaled: false, name: "men_upper_extremity"},
    %{id: 21, scaled: false, name: "women_upper_extremity"},
    %{id: 22, scaled: false, name: "men_lower_extremity"},
    %{id: 23, scaled: false, name: "women_lower_extremity"},
    %{id: 24, scaled: false, name: "men_neuromuscular"},
    %{id: 25, scaled: false, name: "women_neuromuscular"},
    %{id: 26, scaled: false, name: "men_vision"},
    %{id: 27, scaled: false, name: "women_vision"},
    %{id: 28, scaled: false, name: "men_short_stature"},
    %{id: 29, scaled: false, name: "women_short_stature"},
    %{id: 30, scaled: false, name: "men_seated_w_hip"},
    %{id: 31, scaled: false, name: "women_seated_w_hip"},
    %{id: 32, scaled: false, name: "men_seated_wo_hip"},
    %{id: 33, scaled: false, name: "women_seated_wo_hip"},
    %{id: 34, scaled: false, name: "men_intellectual"},
    %{id: 35, scaled: false, name: "women_intellectual"}
  ]

  @scaling [
    %{id: 0, name: "rx"},
    %{id: 1, name: "scaled"},
    %{id: 2, name: "foundations"},
    # %{id: 3, name: "equipment_free"}
  ]

  def run do
    Enum.each(@divisions, fn division ->
      build_data(division)
    end)
  end

  defp build_data(%{scaled: false} = division) do
    file = File.open!("#{division.name}-#{@year}.csv", [:write, :utf8])

    total_pages = division.id |> url(@year, 1) |> total_pages()
    IO.puts("Building non-scaled #{division.name}... #{total_pages} pages found.")

    for page <- 1..total_pages do
      HTTPoison.get!(url(division.id, @year, page), timeout: 10_000)
      |> parse_page(file)
    end
  end

  defp build_data(%{scaled: true} = division) do
    for scale <- @scaling do
      file = File.open!("#{division.name}-#{scale.name}-#{@year}.csv", [:write, :utf8])

      total_pages = division.id |> url(@year, scale.id, 1) |> total_pages()
      IO.puts("Building #{division.name} #{scale.name}... #{total_pages} pages found.")

      for page <- 1..total_pages do
        HTTPoison.get!(url(division.id, @year, scale.id, page), timeout: 10_000)
        |> parse_page(file)
      end
    end
  end

  @base "https://c3po.crossfit.com/api/competitions/v2/competitions/open/"
  defp url(division_id, year, scale, page) do
    "#{@base}#{year}/leaderboards?view=0&division=#{division_id}&region=0&scaled=#{scale}&sort=0&page=#{
      page
    }"
  end

  defp url(division_id, year, page) do
    "#{@base}#{year}/leaderboards?view=0&division=#{division_id}&region=0&scaled=0&sort=0&page=#{
      page
    }"
  end

  defp total_pages(url) do
    %HTTPoison.Response{status_code: 200, body: body} = HTTPoison.get!(url, timeout: 10_000)

    Jason.decode!(body)["pagination"]["totalPages"]
  end

  defp parse_page(%HTTPoison.Response{status_code: 200, body: body}, file)
       when not is_nil(body) do
    case Jason.decode!(body)["leaderboardRows"] do
      nil ->
        nil

      data ->
        data
        |> Enum.map(&parse_athlete/1)
        |> CSV.encode()
        |> Enum.each(&IO.write(file, &1))
    end
  end

  defp parse_page(_error, _file) do
    "couldn't parse page"
  end

  defp parse_athlete(data) do
    workouts =
      Enum.map(data["scores"], &parse_workout/1)
      |> List.flatten()

    [
      data["entrant"]["firstName"],
      data["entrant"]["lastName"],
      data["entrant"]["gender"],
      data["entrant"]["height"],
      data["entrant"]["weight"],
      data["entrant"]["age"],
      data["entrant"]["competitorId"],
      data["entrant"]["competitorName"],
      data["entrant"]["countryOfOriginName"],
      data["entrant"]["countryOfOriginCode"],
      data["entrant"]["affiliateId"],
      data["entrant"]["affiliateName"],
      data["entrant"]["regionId"],
      data["entrant"]["regionName"],
      data["entrant"]["status"],
      data["entrant"]["postCompStatus"],
      data["entrant"]["teamCaptain"],
      data["nextStage"],
      data["overallRank"],
      data["overallScore"]
    ]
    |> List.flatten(workouts)
  end

  defp parse_workout(data) do
    [
      data["ordinal"],
      data["rank"],
      data["score"],
      data["breakdown"],
      data["scoreDisplay"],
      data["scoreIdentifier"],
      data["mobileScoreDisplay"],
      data["scaled"],
      data["affiliate"],
      data["score"],
      data["judge"],
      data["time"],
      data["heat"],
      data["lane"],
      data["video"]
    ]
  end
end
