defmodule OpenScraper.Athlete do
  @moduledoc "Scrape individual athlete data from CF Games site"

  import Meeseeks.CSS

  alias OpenScraper.Missing

  @default_timeout 60_000

  def run do
    Enum.each(missing_data(), &build_data(&1))
  end

  defp athlete_data(page, id) do
    Meeseeks.all(page, css("ul.infobar li .text"))
    |> normalize_athlete_data(id)
  end

  defp normalize_athlete_data([], _id), do: %{}

  defp normalize_athlete_data([a, b, c, d, e, f, g], id) do
    %{
      a: id,
      z0: Meeseeks.text(a),
      z1: Meeseeks.text(b),
      z2: Meeseeks.text(c),
      z3: Meeseeks.text(d),
      z4: Meeseeks.text(e),
      z5: Meeseeks.text(f),
      z6: Meeseeks.text(g)
    }
  end

  defp normalize_athlete_data(error, id) do
    IO.inspect("COULD NOT NORMALIZE ATHLETE DATA ID: #{id} - #{inspect(error)}")
    %{}
  end

  defp build_data({dataset_name, ids}) do
    file = File.open!("missing-#{dataset_name}.csv", [:write, :utf8])

    for athlete_id <- ids do
      HTTPoison.get!(url(athlete_id), timeout: @default_timeout).body
      |> athlete_data(athlete_id)
    end
    |> Enum.reject(&(&1 == %{}))
    |> Enum.map(&Map.values/1)
    |> CSV.encode()
    |> Enum.each(&IO.write(file, &1))
  end

  defp url(athlete_id), do: "https://games.crossfit.com/athlete/#{athlete_id}"

  def missing_data do
    with {:ok, package} <- XlsxReader.open("priv/Missing-Ages.xlsx"),
         # with {:ok, package} <- XlsxReader.open("priv/test_missing.xlsx"),
         {:ok, sheets} <- XlsxReader.sheets(package) do
      Enum.map(sheets, fn {n, [_ | rows]} ->
        {parse_name(n), rows |> Enum.reject(&(&1 == [])) |> Enum.map(&fetch_id/1)}
      end)
    else
      err -> IO.inspect("ERROR PARSING DATA #{inspect(err)}")
    end
  end

  defp fetch_id([id | _]), do: trunc(id)

  defp parse_name(name),
    do: name |> Macro.underscore() |> String.replace(" ", "")
end
