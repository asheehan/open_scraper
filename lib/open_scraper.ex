defmodule OpenScraper do
  @moduledoc """
  Scrap CF Games Open data from
  https://games.crossfit.com/leaderboard/open/2019?country_champions=0&division=1&sort=0&scaled=0&page=1
  """

  @year 2020
  @divisions [
    %{id: 1, name: "men"},
    %{id: 2, name: "women"},
    %{id: 7, name: "men55"},
    %{id: 8, name: "women55"},
    %{id: 9, name: "men60"},
    %{id: 10, name: "women60"},
    %{id: 16, name: "boys16"},
    %{id: 17, name: "girls16"},
    %{id: 14, name: "boys14"},
    %{id: 15, name: "girls14"}
  ]
  def run do
    Enum.each(@divisions, fn division ->
      build_data(division)
    end)
  end

  defp build_data(division) when is_map(division) do
    file = File.open!("#{division.name}-#{@year}.csv", [:write, :utf8])

    total_pages = division.id |> url(@year, 1) |> total_pages()
    IO.puts("Building #{division.name}... #{total_pages} pages found.")

    for page <- 1..total_pages do
      HTTPoison.get!(url(division.id, @year, page), timeout: 10_000)
      |> parse_page(file)
    end
  end

  defp url(division_id, year, page) do
    "https://games.crossfit.com/competitions/api/v1/competitions/open/#{year}/leaderboards?country_champions=0&division=#{
      division_id
    }&sort=0&scaled=0&page=#{page}"
  end

  defp total_pages(url) do
    %HTTPoison.Response{status_code: 200, body: body} = HTTPoison.get!(url, timeout: 10_000)

    Jason.decode!(body)["pagination"]["totalPages"]
  end

  defp parse_page(%HTTPoison.Response{status_code: 200, body: body}, file)
       when not is_nil(body) do
    case Jason.decode!(body)["leaderboardRows"] do
      nil ->
        nil

      data ->
        data
        |> Enum.map(&parse_athlete/1)
        |> CSV.encode()
        |> Enum.each(&IO.write(file, &1))
    end
  end

  defp parse_page(_error, _file) do
    "couldn't parse page"
  end

  defp parse_athlete(data) do
    workouts =
      Enum.map(data["scores"], &parse_workout/1)
      |> List.flatten()

    [
      data["entrant"]["firstName"],
      data["entrant"]["lastName"],
      data["entrant"]["gender"],
      data["entrant"]["height"],
      data["entrant"]["weight"],
      data["entrant"]["age"],
      data["entrant"]["competitorId"],
      data["entrant"]["competitorName"],
      data["entrant"]["countryOfOriginName"],
      data["entrant"]["countryOfOriginCode"],
      data["entrant"]["affiliateId"],
      data["entrant"]["affiliateName"],
      data["overallRank"],
      data["overallScore"]
    ]
    |> List.flatten(workouts)
  end

  defp parse_workout(data) do
    [
      data["ordinal"],
      data["scaled"],
      data["breakdown"],
      data["scoreDisplay"],
      data["affiliate"],
      data["score"],
      data["judge"],
      data["rank"],
      data["time"],
      data["heat"],
      data["lane"],
      data["video"]
    ]
  end
end
